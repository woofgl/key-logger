# -*- coding: UTF-8 -*-

from django.contrib import admin
from models import KeyLog


class KeyLogAdmin(admin.ModelAdmin):

    list_display = ('remoteIp', 'host', 'user_name', 'pwd', 'log_date')
    search_fields = ('remoteIp', 'host', 'user_name', 'pwd', 'log_date')

admin.site.register(KeyLog, KeyLogAdmin)