# -*- coding: UTF-8 -*-
from django.core.exceptions import PermissionDenied
from django.db import router
from django.template.response import TemplateResponse
from django.utils.encoding import force_unicode
from django.template import loader

import xadmin
from django.utils.translation import ugettext as _
from xadmin.plugins.actions import BaseActionView, ACTION_CHECKBOX_NAME
from xadmin.util import model_ngettext, get_deleted_objects
from xadmin.views import filter_hook, BaseAdminPlugin, ListAdminView, BaseAdminView
from xadmin.sites import site
from xadmin.views import BaseAdminPlugin, ListAdminView
from models import KeyLog, UserLog,ExcludeMail


class DeleteSelectedLogActionFromUser(BaseActionView):

    action_name = "delete_log_from_user"
    description = _(u'Delete selected %(verbose_name_plural)s from user')

    delete_confirmation_template = None
    delete_selected_confirmation_template = None

    model_perm = 'view'
    icon = 'remove'

    @filter_hook
    def remove_log_models(self, queryset):
        n = queryset.count()
        if n:
            # queryset.delete()
            # self.user.keylog_set.add(queryset.all())
            for obj in queryset.all():
                self.user.keylog_set.remove(obj)

            self.user.save()
            self.message_user(_("Successfully delete %(count)d %(items)s.") % {
                "count": n, "items": model_ngettext(self.opts, n)
            }, 'success')

    @filter_hook
    def do_action(self, queryset):
        if not SELF_LOG_VAR in self.request.GET:
            return None
        # Check that the user has delete permission for the actual model
        if not self.has_view_permission():
            raise PermissionDenied

        using = router.db_for_write(self.model)

        # Populate deletable_objects, a data structure of all related objects that
        # will also be deleted.
        deletable_objects, perms_needed, protected = get_deleted_objects(
            queryset, self.opts, self.user, self.admin_site, using)

        # The user has already confirmed the deletion.
        # Do the deletion and return a None to display the change list view again.
        if self.request.POST.get('post'):
            if perms_needed:
                raise PermissionDenied
            self.remove_log_models(queryset)
            # Return None to display the change list page again.
            return None

        if len(queryset) == 1:
            objects_name = force_unicode(self.opts.verbose_name)
        else:
            objects_name = force_unicode(self.opts.verbose_name_plural)

        if perms_needed or protected:
            title = _("Cannot delete %(name)s") % {"name": objects_name}
        else:
            title = _("Are you sure?")

        context = self.get_context()
        context.update({
            "title": title,
            "objects_name": objects_name,
            "deletable_objects": [deletable_objects],
            'queryset': queryset,
            "perms_lacking": perms_needed,
            "protected": protected,
            "opts": self.opts,
            "app_label": self.app_label,
            'action_checkbox_name': ACTION_CHECKBOX_NAME,
        })

        # Display the confirmation page
        return TemplateResponse(self.request, self.delete_selected_confirmation_template or
                                self.get_template_list('views/model_delete_selected_from_user_confirm.html'), context, current_app=self.admin_site.name)


class SaveSelectedActionToUser(BaseActionView):

    action_name = "save_selected_to_user"
    description = _(u'Save selected %(verbose_name_plural)s to user')

    delete_confirmation_template = None
    delete_selected_confirmation_template = None

    model_perm = 'view'
    icon = 'save'

    @filter_hook
    def save_user_models(self, queryset):
        n = queryset.count()
        if n:
            # queryset.delete()
            # self.user.keylog_set.add(queryset.all())
            for obj in queryset.all():
                userLog = UserLog()
                userLog.remoteIp = obj.remoteIp
                userLog.host = obj.host
                userLog.user_name = obj.user_name
                userLog.pwd = obj.pwd
                userLog.country = obj.country
                userLog.log_date = obj.log_date
                userLog.log_id = obj.id
                userLog.user = self.user
                userLog.save()


            # self.user.save()
            self.message_user(_("Successfully save %(count)d %(items)s.") % {
                "count": n, "items": model_ngettext(self.opts, n)
            }, 'success')

    @filter_hook
    def do_action(self, queryset):
        # Check that the user has delete permission for the actual model
        if not self.has_view_permission():
            raise PermissionDenied

        using = router.db_for_write(self.model)

        # Populate deletable_objects, a data structure of all related objects that
        # will also be deleted.
        deletable_objects, perms_needed, protected = get_deleted_objects(
            queryset, self.opts, self.user, self.admin_site, using)

        # The user has already confirmed the deletion.
        # Do the deletion and return a None to display the change list view again.
        if self.request.POST.get('post'):
            if perms_needed:
                raise PermissionDenied
            self.save_user_models(queryset)
            # Return None to display the change list page again.
            return None

        if len(queryset) == 1:
            objects_name = force_unicode(self.opts.verbose_name)
        else:
            objects_name = force_unicode(self.opts.verbose_name_plural)

        if perms_needed or protected:
            title = _("Cannot save %(name)s") % {"name": objects_name}
        else:
            title = _("Are you sure?")

        context = self.get_context()
        context.update({
            "title": title,
            "objects_name": objects_name,
            "deletable_objects": [deletable_objects],
            'queryset': queryset,
            "perms_lacking": perms_needed,
            "protected": protected,
            "opts": self.opts,
            "app_label": self.app_label,
            'action_checkbox_name': ACTION_CHECKBOX_NAME,
        })

        # Display the confirmation page
        return TemplateResponse(self.request, self.delete_selected_confirmation_template or
                                self.get_template_list('views/model_save_selected_to_user_confirm.html'), context, current_app=self.admin_site.name)


class KeyLogAdminX(object):
    # self_log = True
    hide_pwd = True

    def open_mail(self, instance):
        return "<a class='opener' href='%s' target='_blank'>%s</a>" % (instance.get_opener_url(), instance.host)

    actions = [SaveSelectedActionToUser]

    open_mail.short_description = "host"
    open_mail.allow_tags = True
    open_mail.is_column = True

    list_display = ('remoteIp', 'open_mail', 'user_name', 'pwd', 'log_date')
    list_filters = ('remoteIp', 'open_mail', 'user_name', 'log_date')
    search_fields = ('remoteIp', 'host', 'user_name',  'log_date')
    list_exclude = ('del_flag',)
    exclude = ['del_flag']
    

class UserLogAdminX(object):
    user_log = True
    hide_pwd = True

    def open_mail(self, instance):
        return "<a class='opener' href='%s' target='_blank'>%s</a>" % (instance.get_opener_url(), instance.host)

    # actions = [SaveSelectedActionToUser, DeleteSelectedLogActionFromUser]

    open_mail.short_description = "host"
    open_mail.allow_tags = True
    open_mail.is_column = True

    list_display = ('remoteIp', 'open_mail', 'user_name', 'pwd', 'log_date')
    list_filters = ('remoteIp', 'open_mail', 'user_name', 'log_date')
    search_fields = ('remoteIp', 'host', 'user_name',  'log_date')
    # list_exclude = ('pwd',)
    list_exclude = ('del_flag',)
    exclude = ['del_flag']

xadmin.site.register(KeyLog, KeyLogAdminX)
xadmin.site.register(UserLog, UserLogAdminX)
xadmin.site.register(ExcludeMail)

SELF_LOG_VAR = '_self_log_'


class SelfLogPlugin(BaseAdminPlugin):
    self_log = False

    def init_request(self, *args, **kwargs):
        return bool(self.self_log)

    # Media
    # def get_media(self, media):
    #     if self.refresh_times and self.request.GET.get(REFRESH_VAR):
    #         media = media + self.vendor('xadmin.plugin.refresh.js')
    #     return media

    # Block Views
    def block_nav_menu(self, context, nodes):
        current_self_log = SELF_LOG_VAR in self.request.GET
        if self.self_log:
            context.update({
                'has_self_log': current_self_log,
                'clean_self_log_url': self.admin_view.get_query_string(remove=(SELF_LOG_VAR,)),
                'self_log_url': self.admin_view.get_query_string({SELF_LOG_VAR: ''}),
            })
            nodes.append(loader.render_to_string('xadmin/blocks/model_list.nav_menu.selflog.html', context_instance=context))

    def get_list_queryset(self, queryset):
        if SELF_LOG_VAR in self.request.GET:
            return UserLog.objects.filter(user__id=self.user.id)
        else:
            return queryset


class UserLogPlugin(BaseAdminPlugin):
    user_log = False

    def init_request(self, *args, **kwargs):
        return bool(self.self_log)

    def get_list_queryset(self, queryset):
        return queryset.filter(user__id=self.user.id)

site.register_plugin(SelfLogPlugin, ListAdminView)


class HidePwdPlugin(BaseAdminPlugin):
    hide_pwd = False

    def init_request(self, *args, **kwargs):
        return bool(self.hide_pwd)

    def get_list_display(self, fields):
        if not self.user.has_perm('mylog.can_view_pwd'):
            if u'pwd' in fields:
                fields.remove(u'pwd')

        return fields

site.register_plugin(HidePwdPlugin, ListAdminView)