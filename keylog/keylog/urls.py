from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
#from django.contrib import admin
import settings
from mylog.views import OpenerView, UserOpenerView
#admin.autodiscover()

import xadmin
xadmin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='base.html')),

    # Examples:
    # url(r'^$', 'keylog.views.home', name='home'),
    # url(r'^keylog/', include('keylog.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    #url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    #url(r'^admin/', include(admin.site.urls)),
    url(r'xadmin/', include(xadmin.site.urls)),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.base.MEDIA_ROOT}),
    url(r'^keylog$', "mylog.views.keylog"),
    url(r'^opener/(?P<pk>\d+)/$', OpenerView.as_view(), name="opener"),
    url(r'^useropener/(?P<pk>\d+)/$', UserOpenerView.as_view(),
        name="useropener"),
    

)
